from django.db import models

# Create your models here.
# You need everytime changes this file, python manage.py makemigrations
# Before you make migrations file, you need to apply changes to database doing python manage.py migrate
class TodoItem(models.Model):
  content = models.TextField()