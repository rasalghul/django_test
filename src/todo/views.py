from django.shortcuts import render
from django.http import HttpResponseRedirect
# To retrieve data from model you need to import model first
from .models import TodoItem

# Create your views here. (similar to controller in rails)

def todo_index_view(request):
  # Retrieve all the items from database and store in a variable
  all_todo_items = TodoItem.objects.all()

  # return HttpResponse('<h1>Hello, this is a todo view.</h1>') # Raw response

  # render requires 3 arguments, response is the response from http, 'todo.html' is the template that will respond, and all_items is the collection of items stored in all_todo_items retrieved from the database
  return render(request, 'todo.html', {'all_items': all_todo_items}) # Render a template (like view in rails)

def add_todo_view(request):
  # Create a new todo all_item
  new_item = TodoItem(content = request.POST['content'])
  # save te object to the database
  new_item.save()
  # redirect the browser '/todo/'
  return HttpResponseRedirect('/todo/')

def delete_todo_view(request, todo_id):
  # Delete item with id recived in the params
  item_to_delete = TodoItem.objects.get(id = todo_id)
  item_to_delete.delete()
  # Redirect the browser '/todo/'
  return HttpResponseRedirect('/todo/')